/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */

import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackCleanupPlugin from 'webpack-cleanup-plugin';
import path from 'path';
import fs from 'fs';

const files = fs.readdirSync(path.join(__dirname, 'src/scripts'));

const htmlPlugins = files.map((file) => {
  const fileName = path.basename(file, '.js');

  let template;
  if (fileName === 'index') {
    template = path.join(__dirname, 'src/templates/index.hbs');
  } else {
    template = path.join(__dirname, 'src/templates/country.hbs');
  }

  return new HtmlWebpackPlugin({
    template,
    inject: true,
    chunks: [fileName, 'vendor'],
    filename: `${fileName}.html`,
    favicon: 'src/images/favicon.ico',
  });
});

const entry = Object.assign({
  vendor: ['geodashboard'],
}, files.reduce((obj, file) => {
  obj[path.basename(file, '.js')] = path.join(__dirname, 'src/scripts', file);
  return obj;
}, {}));

module.exports = {
  entry,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    noParse: /geodashboard/,
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader',
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader']),
      },
      {
        test: /\.(jpg|jpeg|gif|png|ico)$/,
        exclude: /node_modules/,
        loader: 'file-loader',
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('main.css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
    }),
    new CopyWebpackPlugin([{
      from: path.join(__dirname, 'src/locales'),
      to: 'locales',
    }, {
      from: path.join(__dirname, 'src/images'),
      to: 'images',
    }]),
    new webpack.optimize.UglifyJsPlugin({
      exclude: /vendor/,
      compress: {
        warnings: false,
      },
      output: {
        comments: false,
      },
    }),
    new WebpackCleanupPlugin(),
  ].concat(htmlPlugins),
};
