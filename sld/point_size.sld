<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>point_class</se:Name>
    <UserStyle>
      <se:Name>point_class</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>A</se:Name>
          <se:Description>
            <se:Title>A</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>score</ogc:PropertyName>
              <ogc:Literal>A</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#54BA46</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>
                <ogc:Function name="Interpolate">
                  <ogc:PropertyName>population</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
                  <ogc:Literal>10</ogc:Literal>
                  <ogc:Literal>1000</ogc:Literal>
                  <ogc:Literal>20</ogc:Literal>
                  <ogc:Literal>10000</ogc:Literal>
                  <ogc:Literal>50</ogc:Literal>
                  <ogc:Literal>100000</ogc:Literal>
                  <ogc:Literal>80</ogc:Literal>
                </ogc:Function>
              </se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>B</se:Name>
          <se:Description>
            <se:Title>B</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>score</ogc:PropertyName>
              <ogc:Literal>B</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#FFFF39</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>
                <ogc:Function name="Interpolate">
                  <ogc:PropertyName>population</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
                  <ogc:Literal>10</ogc:Literal>
                  <ogc:Literal>1000</ogc:Literal>
                  <ogc:Literal>20</ogc:Literal>
                  <ogc:Literal>10000</ogc:Literal>
                  <ogc:Literal>50</ogc:Literal>
                  <ogc:Literal>100000</ogc:Literal>
                  <ogc:Literal>80</ogc:Literal>
                </ogc:Function>
              </se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>C</se:Name>
          <se:Description>
            <se:Title>C</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>score</ogc:PropertyName>
              <ogc:Literal>C</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#F35620</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>
                <ogc:Function name="Interpolate">
                  <ogc:PropertyName>population</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
                  <ogc:Literal>10</ogc:Literal>
                  <ogc:Literal>1000</ogc:Literal>
                  <ogc:Literal>20</ogc:Literal>
                  <ogc:Literal>10000</ogc:Literal>
                  <ogc:Literal>50</ogc:Literal>
                  <ogc:Literal>100000</ogc:Literal>
                  <ogc:Literal>80</ogc:Literal>
                </ogc:Function>
              </se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>D</se:Name>
          <se:Description>
            <se:Title>D</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>score</ogc:PropertyName>
              <ogc:Literal>D</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#C92429</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>
                <ogc:Function name="Interpolate">
                  <ogc:PropertyName>population</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
                  <ogc:Literal>10</ogc:Literal>
                  <ogc:Literal>1000</ogc:Literal>
                  <ogc:Literal>20</ogc:Literal>
                  <ogc:Literal>10000</ogc:Literal>
                  <ogc:Literal>50</ogc:Literal>
                  <ogc:Literal>100000</ogc:Literal>
                  <ogc:Literal>80</ogc:Literal>
                </ogc:Function>
              </se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>None</se:Name>
          <se:Description>
            <se:Title>None</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>score</ogc:PropertyName>
              <ogc:Literal></ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#ffffff</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>
                <ogc:Function name="Interpolate">
                  <ogc:PropertyName>population</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
                  <ogc:Literal>10</ogc:Literal>
                  <ogc:Literal>1000</ogc:Literal>
                  <ogc:Literal>20</ogc:Literal>
                  <ogc:Literal>10000</ogc:Literal>
                  <ogc:Literal>50</ogc:Literal>
                  <ogc:Literal>100000</ogc:Literal>
                  <ogc:Literal>80</ogc:Literal>
                </ogc:Function>
              </se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
