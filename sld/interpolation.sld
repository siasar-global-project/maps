<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>data:communities</Name>
    <UserStyle>
      <Title>Barnes Surface</Title>
      <Abstract>A style that produces a Barnes surface using a rendering transformation</Abstract>
      <FeatureTypeStyle>
        <Transformation>
          <ogc:Function name="vec:BarnesSurface">
            <ogc:Function name="parameter">
              <ogc:Literal>data</ogc:Literal>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>valueAttr</ogc:Literal>
              <ogc:Literal>wsp</ogc:Literal>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>scale</ogc:Literal>
              <ogc:Function name="Categorize">
                <ogc:Function name="env">
                  <ogc:Literal>wms_scale_denominator</ogc:Literal>
                </ogc:Function>
                <ogc:Literal>0.003</ogc:Literal>
                <ogc:Literal>1000000</ogc:Literal> <!-- zoom 9 -->
                <ogc:Literal>0.007</ogc:Literal>
                <ogc:Literal>2000000</ogc:Literal> <!-- zoom 8 -->
                <ogc:Literal>0.01</ogc:Literal>
                <ogc:Literal>4000000</ogc:Literal> <!-- zoom 7 -->
                <ogc:Literal>0.05</ogc:Literal>
                <ogc:Literal>8000000</ogc:Literal> <!-- zoom 6 -->
                <ogc:Literal>0.06</ogc:Literal>
                <ogc:Literal>30000000</ogc:Literal> <!-- zoom 4 -->
                <ogc:Literal>0.1</ogc:Literal>
                <ogc:Literal>130000000</ogc:Literal> <!-- zoom 2 -->
                <ogc:Literal>0.5</ogc:Literal>
                <ogc:Literal>270000000</ogc:Literal> <!-- zoom 1 -->
                <ogc:Literal>0.8</ogc:Literal>
                <ogc:Literal>550000000</ogc:Literal> <!-- zoom 0 -->
                <ogc:Literal>1.1</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>convergence</ogc:Literal>
              <ogc:Literal>0.3</ogc:Literal>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>passes</ogc:Literal>
              <ogc:Literal>1</ogc:Literal>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>minObservations</ogc:Literal>
              <ogc:Function name="Categorize">
                <ogc:Function name="env">
                  <ogc:Literal>wms_scale_denominator</ogc:Literal>
                </ogc:Function>
                <ogc:Literal>1</ogc:Literal>
                <ogc:Literal>1000000</ogc:Literal> <!-- zoom 9 -->
                <ogc:Literal>2</ogc:Literal>
                <ogc:Literal>4000000</ogc:Literal> <!-- zoom 7 -->
                <ogc:Literal>5</ogc:Literal>
                <ogc:Literal>8000000</ogc:Literal> <!-- zoom 6 -->
                <ogc:Literal>5</ogc:Literal>
                <ogc:Literal>30000000</ogc:Literal> <!-- zoom 4 -->
                <ogc:Literal>10</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>maxObservationDistance</ogc:Literal>
              <ogc:Function name="Categorize">
                <ogc:Function name="env">
                  <ogc:Literal>wms_scale_denominator</ogc:Literal>
                </ogc:Function>
                <ogc:Literal>0.1</ogc:Literal>
                <ogc:Literal>1000000</ogc:Literal> <!-- zoom 9 -->
                <ogc:Literal>0.2</ogc:Literal>
                <ogc:Literal>2000000</ogc:Literal> <!-- zoom 8 -->
                <ogc:Literal>0.3</ogc:Literal>
                <ogc:Literal>4000000</ogc:Literal> <!-- zoom 7 -->
                <ogc:Literal>0.5</ogc:Literal>
                <ogc:Literal>8000000</ogc:Literal> <!-- zoom 6 -->
                <ogc:Literal>0.8</ogc:Literal>
                <ogc:Literal>30000000</ogc:Literal> <!-- zoom 4 -->
                <ogc:Literal>1.8</ogc:Literal>
                <ogc:Literal>130000000</ogc:Literal> <!-- zoom 2 -->
                <ogc:Literal>3</ogc:Literal>
                <ogc:Literal>270000000</ogc:Literal> <!-- zoom 1 -->
                <ogc:Literal>4</ogc:Literal>
                <ogc:Literal>550000000</ogc:Literal> <!-- zoom 0 -->
                <ogc:Literal>7</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>pixelsPerCell</ogc:Literal>
              <ogc:Function name="Categorize">
                <ogc:Function name="env">
                  <ogc:Literal>wms_scale_denominator</ogc:Literal>
                </ogc:Function>
                <ogc:Literal>50</ogc:Literal>
                <ogc:Literal>600000</ogc:Literal> <!-- zoom 10 -->
                <ogc:Literal>40</ogc:Literal>
                <ogc:Literal>1000000</ogc:Literal> <!-- zoom 9 -->
                <ogc:Literal>35</ogc:Literal>
                <ogc:Literal>2000000</ogc:Literal> <!-- zoom 8 -->
                <ogc:Literal>25</ogc:Literal>
                <ogc:Literal>4000000</ogc:Literal> <!-- zoom 7 -->
                <ogc:Literal>17</ogc:Literal>
                <ogc:Literal>8000000</ogc:Literal> <!-- zoom 6 -->
                <ogc:Literal>12</ogc:Literal>
                <ogc:Literal>30000000</ogc:Literal> <!-- zoom 4 -->
                <ogc:Literal>9</ogc:Literal>
                <ogc:Literal>130000000</ogc:Literal> <!-- zoom 2 -->
                <ogc:Literal>8</ogc:Literal>
                <ogc:Literal>270000000</ogc:Literal> <!-- zoom 1 -->
                <ogc:Literal>7</ogc:Literal>
                <ogc:Literal>550000000</ogc:Literal> <!-- zoom 0 -->
                <ogc:Literal>6</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>queryBuffer</ogc:Literal>
              <ogc:Function name="Categorize">
                <ogc:Function name="env">
                  <ogc:Literal>wms_scale_denominator</ogc:Literal>
                </ogc:Function>
                <ogc:Literal>0.009</ogc:Literal>
                <ogc:Literal>1000000</ogc:Literal> <!-- zoom 9 -->
                <ogc:Literal>0.021</ogc:Literal>
                <ogc:Literal>2000000</ogc:Literal> <!-- zoom 8 -->
                <ogc:Literal>0.03</ogc:Literal>
                <ogc:Literal>4000000</ogc:Literal> <!-- zoom 7 -->
                <ogc:Literal>0.15</ogc:Literal>
                <ogc:Literal>8000000</ogc:Literal> <!-- zoom 6 -->
                <ogc:Literal>0.18</ogc:Literal>
                <ogc:Literal>30000000</ogc:Literal> <!-- zoom 4 -->
                <ogc:Literal>0.3</ogc:Literal>
                <ogc:Literal>130000000</ogc:Literal> <!-- zoom 2 -->
                <ogc:Literal>1.5</ogc:Literal>
                <ogc:Literal>270000000</ogc:Literal> <!-- zoom 1 -->
                <ogc:Literal>2.4</ogc:Literal>
                <ogc:Literal>550000000</ogc:Literal> <!-- zoom 0 -->
                <ogc:Literal>3.3</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>outputBBOX</ogc:Literal>
              <ogc:Function name="env">
                <ogc:Literal>wms_bbox</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>outputWidth</ogc:Literal>
              <ogc:Function name="env">
                <ogc:Literal>wms_width</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
            <ogc:Function name="parameter">
              <ogc:Literal>outputHeight</ogc:Literal>
              <ogc:Function name="env">
                <ogc:Literal>wms_height</ogc:Literal>
              </ogc:Function>
            </ogc:Function>
          </ogc:Function>
        </Transformation>
        <Rule>
          <RasterSymbolizer>
            <!-- specify geometry attribute of input to pass validation -->
            <Geometry>
              <ogc:PropertyName>point</ogc:PropertyName>
            </Geometry>
            <Opacity>1</Opacity>
            <ColorMap type="ramp">
              <ColorMapEntry color="#FFFFFF" quantity="-1" label="nodata" opacity="0" />
              <ColorMapEntry color="#C92429" quantity="0" label="D" />
              <ColorMapEntry color="#FF9326" quantity="0.4" label="C" />
              <ColorMapEntry color="#FFFF39" quantity="0.7" label="B" />
              <ColorMapEntry color="#54BA46" quantity="1" label="A" />
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
