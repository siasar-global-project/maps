import * as GeoDashboard from 'geodashboard';
import SiasarDashboard from '../lib/siasar-dashboard';

const dashboard = new SiasarDashboard({
  dashboard: {
    map: {
      center: [-80, 10],
      zoom: 4,
    },
  },
});

dashboard.addFilter(new GeoDashboard.Filter({
  property: [new GeoDashboard.Filter({
    property: 'country',
    operator: '<>',
    value: 'NI',
  }), new GeoDashboard.Filter({
    property: 'siasar_version',
    operator: '<>',
    value: '1',
    logicalOperator: 'OR',
  })],
}));

dashboard.init().then(() => {
  dashboard.addBaseLayers();
  dashboard.addOverlayLayers();
  dashboard.addWidgets();
  dashboard.render();
});
