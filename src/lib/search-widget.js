import { Widget } from 'geodashboard';

import template from '../templates/search-widget.hbs';
import optionsTemplate from '../templates/search-options.hbs';

import '../styles/search-widget.scss';

class SearchWidget extends Widget {
  constructor(config) {
    super(config);

    this.template = template;
    this.className = 'search-widget';

    this.placeholder = config.placeholder;
  }

  render() {
    super.render();
    this.content.innerHTML = this.template({
      placeholder: this.placeholder,
    });

    this.searchForm = document.getElementById('search');
    this.searchInput = document.getElementById('terms');
    this.searchList = document.getElementById('searchList');

    this.searchForm.addEventListener('submit', (event) => {
      event.preventDefault();
      this.searchList.innerHTML = '';
      if (this.searchInput.value !== '') {
        this.value = this.searchInput.value;
        this.search();
      }
    });

    this.searchList.addEventListener('change', () => {
      this.find();
    });
  }

  refresh() {
    // Just to override super function
  }

  search() {
    const options = {};
    const regex = new RegExp(this.value, 'i');

    this.dashboard.activeLayer.source.forEachFeatureInExtent(this.dashboard.extent, (feature) => {
      if (regex.test(feature.get('name'))) {
        options[feature.getId()] = {
          name: feature.get('name'),
          adm: feature.get('adm_3') || feature.get('adm_2') || feature.get('adm_1') || feature.get('adm_0'),
        };
      }
    });

    this.searchList.innerHTML = optionsTemplate({
      options,
    });
  }

  find() {
    const feature = this.dashboard.activeLayer.source.getFeatureById(this.searchList.value);
    if (this.dashboard.mapZoom > 10) {
      this.dashboard.mapZoom = 10;
    }
    this.dashboard.centerMapToFeature(feature);
    setTimeout(() => {
      this.dashboard.mapZoom = 14;
      setTimeout(() => {
        this.dashboard.showFeaturePopup(this.dashboard.activeLayer, feature);
      }, 2000);
    }, 1000);
  }
}

export default SearchWidget;
