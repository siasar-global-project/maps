import deepAssign from 'deep-assign';
import * as GeoDashboard from 'geodashboard';

import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

import localConfig from '../config/config.json';

import FilterWidget from './filter-widget';
import SearchWidget from './search-widget';

import '../styles/main.scss';

export default class SiasarDashboard extends GeoDashboard.Dashboard {
  constructor(config) {
    config = deepAssign({
      categories: {
        property: 'score',
        values: {
          A: { color: '#54BA46' },
          B: { color: '#FFFF39' },
          C: { color: '#FF9326' },
          D: { color: '#C92429' },
        },
        minRadius: 5,
        maxRadius: 10,
      },
      dashboard: {
        container: document.getElementsByClassName('content')[0],
        filters: [new GeoDashboard.Filter({
          property: 'score',
          operator: '<>',
          value: 'N',
        })],
        map: {
          viewParams: {
            minZoom: 0,
            maxZoom: 18,
          },
        },
      },
      i18n: {
        fallbackLng: 'en',
        backend: {
          loadPath: '/locales/{{lng}}.json?v=1.0',
        },
        detection: {
          order: ['querystring', 'navigator', 'htmlTag'],
          lookupQuerystring: 'l',
        },
      },
    }, config, localConfig);
    super(config.dashboard);
    this.config = config;

    this.i18n = i18n.use(XHR).use(LanguageDetector);

    this.mapManager.once('loaded', this.autoZoom.bind(this));
  }

  init() {
    return new Promise((resolve) => {
      this.i18n.init(this.config.i18n, (error, t) => {
        if (error) console.warn(error);
        resolve(t);
      });
    });
  }

  addBaseLayers() {
    this.addBaseLayer(new GeoDashboard.OSMLayer({
      visible: true,
    }));

    this.addBaseLayer(new GeoDashboard.BingLayer({
      visible: false,
      key: this.config.bing_key,
    }));
  }

  buildReportUrl(type, id) {
    return `${this.config.pentaho.base_url}${this.config.pentaho.reports[type]}/viewer?userid=${this.config.pentaho.username}&password=${this.config.pentaho.password}&id=${id}`;
  }

  buildReportDataUrl(type, id) {
    return `${this.config.namespace.url}/${type}/${id}`;
  }

  addOverlayLayers(config) {
    config = deepAssign({
      adm: {
        title: this.i18n.t('adm'),
        server: `${this.config.server}/gwc/service`,
        layer: `${this.config.namespace.name}:${this.config.layers.adm}`,
        tiled: true,
        exclusive: false,
        visible: false,
        opacity: 0.7,
        attribution: this.config.attribution,
        filters: [],
        layerParams: {
          minResolution: 100, // Hide layer when zoom > 10
        },
      },
      communities: {
        title: this.i18n.t('communities'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.community}`,
        visible: true,
        exclusive: true,
        popup: [{
          property: 'picture_url',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${value}"><img src="${value}"/></a>`;
          },
        }, {
          title: this.i18n.t('name'),
          property: 'name',
        }, {
          title: this.i18n.t('location'),
          property: ['adm_3', 'adm_2', 'adm_1'],
          format: (...adms) => Array.from(adms).filter(n => n).join(', '),
        }, {
          title: this.i18n.t('gps'),
          property: ['latitude', 'longitude'],
          format: (lat, lng) => `${lat.toFixed(5)}, ${lng.toFixed(5)}`,
        }, {
          title: `${this.i18n.t('population')} / ${this.i18n.t('households')}`,
          property: ['population', 'households'],
          format: (population, households) => `${population} / ${households}`,
        }, {
          title: this.i18n.t('shl_ssl'),
          property: 'shl_ssl',
          format: value => (value ? `${(value * 100).toFixed(2)}%` : null),
        }, {
          title: this.i18n.t('wsl_acc'),
          property: 'wsl_acc',
          format: value => (value ? `${(value * 100).toFixed(2)}%` : null),
        }, {
          property: 'siasar_id',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${this.buildReportDataUrl('communities', value)}">${this.i18n.t('more_information')}</a>`;
          },
        }],
        style: this.config.categories,
        attribution: this.config.attribution,
      },
      systems: {
        title: this.i18n.t('systems'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.system}`,
        exclusive: true,
        popup: [{
          property: 'picture_url',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${value}"><img src="${value}"/></a>`;
          },
        }, {
          title: this.i18n.t('name'),
          property: 'name',
        }, {
          title: this.i18n.t('location'),
          property: ['adm_3', 'adm_2', 'adm_1'],
          format: (...adms) => Array.from(adms).filter(n => n).join(', '),
        }, {
          title: this.i18n.t('gps'),
          property: ['latitude', 'longitude'],
          format: (lat, lng) => `${lat.toFixed(5)}, ${lng.toFixed(5)}`,
        }, {
          title: this.i18n.t('building_date'),
          property: 'building_year',
        }, {
          title: this.i18n.t('served_households'),
          property: 'served_households',
        }, {
          property: 'siasar_id',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${this.buildReportDataUrl('systems', value)}">${this.i18n.t('more_information')}</a>`;
          },
        }],
        style: this.config.categories,
        attribution: this.config.attribution,
      },
      service_providers: {
        title: this.i18n.t('service_providers'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.service_provider}`,
        exclusive: true,
        popup: [{
          property: 'picture_url',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${value}"><img src="${value}"/></a>`;
          },
        }, {
          title: this.i18n.t('name'),
          property: 'name',
        }, {
          title: this.i18n.t('location'),
          property: ['adm_3', 'adm_2', 'adm_1'],
          format: (...adms) => Array.from(adms).filter(n => n).join(', '),
        }, {
          title: this.i18n.t('gps'),
          property: ['latitude', 'longitude'],
          format: (lat, lng) => `${lat.toFixed(5)}, ${lng.toFixed(5)}`,
        }, {
          title: this.i18n.t('women'),
          property: 'women_count',
        }, {
          title: this.i18n.t('legal_status'),
          property: 'legal_status',
        }, {
          title: this.i18n.t('provider_type'),
          property: 'provider_type',
        }, {
          title: this.i18n.t('served_households'),
          property: 'served_households',
        }, {
          property: 'siasar_id',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${this.buildReportDataUrl('service-providers', value)}">${this.i18n.t('more_information')}</a>`;
          },
        }],
        style: this.config.categories,
        attribution: this.config.attribution,
      },
      technical_providers: {
        title: this.i18n.t('technical_providers'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.technical_provider}`,
        exclusive: true,
        popup: [{
          title: this.i18n.t('name'),
          property: 'name',
        }, {
          title: this.i18n.t('location'),
          property: ['adm_3', 'adm_2', 'adm_1'],
          format: (...adms) => Array.from(adms).filter(n => n).join(', '),
        }, {
          title: this.i18n.t('gps'),
          property: ['latitude', 'longitude'],
          format: (lat, lng) => `${lat.toFixed(5)}, ${lng.toFixed(5)}`,
        }, {
          title: this.i18n.t('provider_type'),
          property: 'provider_type',
        }, {
          title: this.i18n.t('served_households'),
          property: 'served_households',
        }, {
          property: 'siasar_id',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${this.buildReportDataUrl('technical-providers', value)}">${this.i18n.t('more_information')}</a>`;
          },
        }],
        style: this.config.categories,
        attribution: this.config.attribution,
      },
      schools: {
        title: this.i18n.t('schools'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.schools}`,
        exclusive: true,
        popup: [{
          title: this.i18n.t('name'),
          property: 'name',
        }, {
          title: this.i18n.t('location'),
          property: ['adm_3', 'adm_2', 'adm_1'],
          format: (...adms) => Array.from(adms).filter(n => n).join(', '),
        }, {
          title: this.i18n.t('gps'),
          property: ['latitude', 'longitude'],
          format: (lat, lng) => `${lat.toFixed(5)}, ${lng.toFixed(5)}`,
        }, {
          title: `${this.i18n.t('teachers')} / ${this.i18n.t('students')}`,
          property: ['teachers', 'students'],
          format: (teachers, students) => `${teachers} / ${students}`,
        }, {
          title: this.i18n.t('have_system'),
          property: 'have_system',
          format: haveSystem => (haveSystem === 'yes' ? this.i18n.t('yes') : this.i18n.t('no')),
        }, {
          title: this.i18n.t('sufficient_washing_facilities'),
          property: 'sufficient_washing_facilities',
          format: swf => (swf === 'yes' ? this.i18n.t('yes') : this.i18n.t('no')),
        }, {
          property: 'siasar_id',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${this.buildReportDataUrl('schools', value)}">${this.i18n.t('more_information')}</a>`;
          },
        }],
        style: this.config.categories,
        attribution: this.config.attribution,
      },
      health_centers: {
        title: this.i18n.t('health_centers'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.health_centers}`,
        exclusive: true,
        popup: [{
          title: this.i18n.t('name'),
          property: 'name',
        }, {
          title: this.i18n.t('location'),
          property: ['adm_3', 'adm_2', 'adm_1'],
          format: (...adms) => Array.from(adms).filter(n => n).join(', '),
        }, {
          title: this.i18n.t('gps'),
          property: ['latitude', 'longitude'],
          format: (lat, lng) => `${lat.toFixed(5)}, ${lng.toFixed(5)}`,
        }, {
          title: this.i18n.t('have_system'),
          property: 'have_system',
          format: haveSystem => (haveSystem === 'yes' ? this.i18n.t('yes') : this.i18n.t('no')),
        }, {
          title: this.i18n.t('sufficient_washing_facilities'),
          property: 'sufficient_washing_facilities',
          format: swf => (swf === 'yes' ? this.i18n.t('yes') : this.i18n.t('no')),
        }, {
          property: 'siasar_id',
          format: (value) => {
            if (!value) return null;
            return `<a target="_blank" href="${this.buildReportDataUrl('health-centers', value)}">${this.i18n.t('more_information')}</a>`;
          },
        }],
        style: this.config.categories,
        attribution: this.config.attribution,
      },
      interpolation: {
        title: this.i18n.t('interpolation'),
        server: `${this.config.server}/gwc/service`,
        layer: `${this.config.namespace.name}:${this.config.layers.community}`,
        tiled: true,
        style: 'data:interpolation',
        exclusive: false,
        visible: false,
        opacity: 0.7,
        attribution: this.config.attribution,
        layerParams: {
          minResolution: 100, // Hide layer when zoom > 10
        },
      },
      heatmap: {
        title: this.i18n.t('heatmap'),
        server: this.config.server,
        layer: `${this.config.namespace.name}:${this.config.layers.community}`,
        style: 'data:heatmap',
        exclusive: false,
        visible: false,
        opacity: 0.75,
        attribution: this.config.attribution,
        layerParams: {
          minResolution: 100, // Hide layer when zoom > 10
        },
      },
    }, config);

    if (config.adm) {
      this.addOverlayLayer(new GeoDashboard.WMSLayer(config.adm));
    }

    if (config.interpolation) {
      this.addOverlayLayer(new GeoDashboard.WMSLayer(config.interpolation));
    }

    if (config.heatmap) {
      this.addOverlayLayer(new GeoDashboard.WMSLayer(config.heatmap));
    }

    if (config.communities) {
      this.addOverlayLayer(new GeoDashboard.WFSLayer(config.communities));
    }

    if (config.systems) {
      this.addOverlayLayer(new GeoDashboard.WFSLayer(config.systems));
    }

    if (config.service_providers) {
      this.addOverlayLayer(new GeoDashboard.WFSLayer(config.service_providers));
    }

    if (config.technical_providers) {
      this.addOverlayLayer(new GeoDashboard.WFSLayer(config.technical_providers));
    }
    if (config.schools) {
      this.addOverlayLayer(new GeoDashboard.WFSLayer(config.schools));
    }
    if (config.health_centers) {
      this.addOverlayLayer(new GeoDashboard.WFSLayer(config.health_centers));
    }
  }

  addWidgets(config) {
    config = deepAssign({
      search: {
        title: this.i18n.t('search.title'),
        placeholder: this.i18n.t('search.placeholder'),
      },
      filter: {
        title: this.i18n.t('filter.title'),
      },
      population: {
        title: this.i18n.t('total_population'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.community}`,
        property: 'population',
        function: 'Sum',
        format: value => parseInt(value, 10) || '--',
      },
      households: {
        title: this.i18n.t('total_households'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.community}`,
        property: 'households',
        function: 'Sum',
        format: value => parseInt(value, 10) || '--',
      },
      communities: {
        title: this.i18n.t('communities'),
        totalLabel: this.i18n.t('total'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.community}`,
        property: 'siasar_id',
        categories: this.config.categories,
      },
      systems: {
        title: this.i18n.t('systems'),
        totalLabel: this.i18n.t('total'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.system}`,
        property: 'siasar_id',
        categories: this.config.categories,
      },
      service_providers: {
        title: this.i18n.t('service_providers'),
        totalLabel: this.i18n.t('total'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.service_provider}`,
        property: 'siasar_id',
        categories: this.config.categories,
      },
      technical_providers: {
        title: this.i18n.t('technical_providers'),
        totalLabel: this.i18n.t('total'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.technical_provider}`,
        property: 'siasar_id',
        categories: this.config.categories,
      },
      schools: {
        title: this.i18n.t('schools'),
        totalLabel: this.i18n.t('total'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.schools}`,
        property: 'siasar_id',
        categories: this.config.categories,
      },
      health_centers: {
        title: this.i18n.t('health_centers'),
        totalLabel: this.i18n.t('total'),
        server: this.config.server,
        namespace: this.config.namespace,
        layer: `${this.config.namespace.name}:${this.config.layers.health_centers}`,
        property: 'siasar_id',
        categories: this.config.categories,
      },
    }, config);

    if (config.search) {
      this.addWidget(new SearchWidget(config.search));
    }

    if (config.filter) {
      this.addWidget(new FilterWidget(config.filter));
    }

    if (config.population) {
      this.addWidget(new GeoDashboard.AggregateWidget(config.population));
    }

    if (config.households) {
      this.addWidget(new GeoDashboard.AggregateWidget(config.households));
    }

    this.addWidget(new GeoDashboard.AggregateWidget({
      title: this.i18n.t('wsl_acc'),
      server: this.config.server,
      namespace: this.config.namespace,
      layer: `${this.config.namespace.name}:${this.config.layers.community}`,
      property: 'wsl_acc',
      function: 'Average',
      format: value => (value ? `${(value * 100).toFixed(2)}%` : '--'),
    }));

    this.addWidget(new GeoDashboard.AggregateWidget({
      title: this.i18n.t('shl_ssl'),
      server: this.config.server,
      namespace: this.config.namespace,
      layer: `${this.config.namespace.name}:${this.config.layers.community}`,
      property: 'shl_ssl',
      function: 'Average',
      format: value => (value ? `${(value * 100).toFixed(2)}%` : '--'),
    }));

    if (config.communities) {
      this.addWidget(new GeoDashboard.CategoryWidget(config.communities));
    }

    if (config.systems) {
      this.addWidget(new GeoDashboard.CategoryWidget(config.systems));
    }

    if (config.service_providers) {
      this.addWidget(new GeoDashboard.CategoryWidget(config.service_providers));
    }

    if (config.technical_providers) {
      this.addWidget(new GeoDashboard.CategoryWidget(config.technical_providers));
    }

    if (config.schools) {
      this.addWidget(new GeoDashboard.CategoryWidget(config.schools));
    }

    if (config.health_centers) {
      this.addWidget(new GeoDashboard.CategoryWidget(config.health_centers));
    }
  }

  autoZoom() {
    this.activeLayer.fitMap();
  }

  get activeLayer() {
    return this.mapManager.overlayLayers.filter(layer => layer.getVisible())[0];
  }
}
